$(document).ready(function(){
  //Hamburger effects
  $('button.hamburger').click(function() {
    $('button.hamburger').toggleClass('is-active');
    $('.options-mobile').toggleClass('active');
  });
  //navbar style while scrolled
  $(window).scroll(function() {
    if ($(document).scrollTop() > 20) {
      $('.navbar-261').addClass('scrolled');
      $(".navbar-261-mobile").addClass("scrolled");
      $('img.logo').attr('src','img/logo-color.png');
      $('img#down-chevron-img').attr('src','img/down-chevron-black.svg');
    } else {
      $('.navbar-261').removeClass('scrolled');
      $(".navbar-261-mobile").removeClass("scrolled");
      $('img.logo').attr('src','img/logo-white.png');
      $('img#down-chevron-img').attr('src', 'img/down-chevron.svg');
    }
  });
  //language selector
  $('ul.lang-select-dropdown-title').click(function(){
    $('li.dropdown-option').toggleClass('dropped');
  });
  
  $('ul.lang-select-dropdown-title.li.a,lang-switch').click(function(){
    $('li.dropdown-option').toggleClass('dropped');
  });
  //Text change on language selector
  $('ul.lang-select-dropdown-title.li.option-1.a.lang-switch').click(function () {
    $('ul.lang-select-dropdown-title').text('ES');
  });
  $('ul.lang-select-dropdown-title.li.option-2.a.lang-switch').click(function(){
    $('ul.lang-select-dropdown-title').text('EN');
  });
  $('ul.lang-select-dropdown-title.li.option-3.a.lang-switch').click(function () {
    $('ul.lang-select-dropdown-title').text('FR');
  });


  
  //To develop later for correct integration
  $('.service-container>a#services-show-more-1').click(function(){
    $('.service-expand-text-1').toggleClass('active-text-1');   
  });
  $('.service-container>a#services-show-more-2').click(function(){
    $('.service-expand-text-2').toggleClass('active-text-2');   
  });
  $('.service-container>a#services-show-more-3').click(function(){
    $('.service-expand-text-3').toggleClass('active-text-3');   
  });
  $('.service-container>a#services-show-more-4').click(function(){
    $('.service-expand-text-4').toggleClass('active-text-4');   
  });
  //Apply button
  $('#apply-button').click(function(){
    $('.apply-button-expand-text').toggleClass('active-text-apply-button');
  });
  //options animations
  $('.options-mobile>li>a').click(function(){
    $('.options-mobile').toggleClass('active');
    $('button.hamburger').toggleClass('is-active');
  });
});