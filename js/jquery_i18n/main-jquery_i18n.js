jQuery(document).ready(function () {
    var update_texts = function () {
       $("#typed").remove();
       $("#TypedAnimated").append("<h2 class='wow fadeInDown' id='typed'></h2>");
        if ($.i18n().locale.includes('es')) {
            var typed3 = new Typed('#typed', {
                strings: ['Para Transformar digitalmente tu <strong>Negocio</strong>', 'Para Transformar digitalmente tu <strong>Producto</strong>', 'Para Transformar digitalmente tu <strong>Emprendimiento</strong>'],
                typeSpeed: 40,
                backSpeed: 10,
                backDelay: 1000,
                startDelay: 400,
                showCursor: false,
                smartBackspace: true, // this is a default
                preStringTyped: function (index, self) {
                    if (index === 3) {
                        self.stop()
                    }

                },
                loop: true
            });
            typed3.reset();
        } else if ($.i18n().locale.includes('en')) {
            var typed3 = new Typed('#typed', {
                strings: ['To Transform digitally your  <strong>Business</strong>', 'To Transform digitally your  <strong>Product</strong>', 'To Transform digitally your  <strong>Startup</strong>'],
                typeSpeed: 40,
                backSpeed: 10,
                backDelay: 1000,
                startDelay: 400,
                showCursor: false,
                smartBackspace: true, // this is a default
                loop: true
            });
            typed3.reset();
        } else {
            console.log('entro en defeaeult');
            var typed3 = new Typed('#typed', {

                strings: ['To Transform digitally your  <strong>Business</strong>', 'To Transform digitally your   <strong>Product</strong>', 'To Transform digitally your  <strong>Startup</strong>'],
                typeSpeed: 40,
                backSpeed: 10,
                backDelay: 1000,
                startDelay: 400,
                showCursor: false,
                smartBackspace: true, // this is a default
                loop: true
            });
            typed3.reset();
        }


        $('body').i18n();
        $('#messages').text($.i18n('message_from', 'Ann', 2, 'female'));
    };

    $('.lang-switch').click(function (e) {

        e.preventDefault();
        $.i18n().locale = $(this).data('locale');
        update_texts();
    });

    $.i18n().load({
        'en': {
            'Servicio': 'Services',
            'Lenguaje': 'EN',
            'Herramientas': 'Technologies',
            'Quienes-somos': 'About',
            'Carreras': 'Careers',
            'Contactanos': 'Contact Us',
            'Soluciones': 'Technological',
            'Tecnologicas': 'Solutions',
            'Servicios': 'Services',            
            'textServicios': 'Either for a small or a large project or to simply extend your internal development team,  261ITSoutions offers you a wide range of services to satisfy your business needs anytime. We can build your product from scratch or improve significantly your existing ones.',
            'Outsourcing-de-Profesionales': 'Professionals Outsourcing',
            'textOutsourcing': 'Members of our team are assigned to a particular client on the areas of Software Development and IT Infrastructure to take care of critical tasks, increasing the productivity of our clients internal teams.',
            'Desarrollo-Web': ' Web Development',
            'TextDesarrollo-Web': 'Web applications development based on usability and responsive design to offer an experience focused entirely on the user.',
            'Desarrollo-Movil': 'Mobile Development',
            'TextDesarrollo-Movil': 'Design and Development of native and hybrid mobile apps by experienced developers on iOS and Android.',
            'QA': 'QA (Software Testing)',
            'TextQA': 'Automation and execution of agile tests to assure the quality and the proper behavior of the applications developed by us and the ones developed by our clients.',
            'UXUI': 'UX/UI',
            'textUXUI': 'Through strategy, design, and technology, we create beautifully crafted websites, apps, and brand experiences that engage today’s consumer',
            'BusinessIntelligence': 'Business Intelligence',
            'TextBusinessIntelligence': 'We help our clients build competitive advantages by strengthening their business strategies and decision making, all based on the correct and appropriate understanding of the true value of the data.',
            'titleServicios2': 'Additional Services',
            'textServicios2': 'Text of Additional Services',
            'MarketStudy': 'Market Study',
            'textMarketStudy': 'Text of Market Study',
            'DigitalMarketing': 'Digital Marketing',
            'textDigitalMarketing': 'Text of Digital Marketing',
            'TextHerramientas': 'Our Developers, Designers, QA Analysts and IT Infrastructure specialists are experts on using the right tools that allow them to build technological solutions that accelerate the growth and the digital transformation of our clients.',
            'Amplia-experiencia': 'Great experience',
            'UX/UI': 'on UX/UI Design and Development focused on',
            'MóvilYWeb': 'Web and Mobile platforms',
            'Somos0': ' We are a ',
            'Somos1': '',
            'Somos2': ' IT Solutions company. We have',
            'Somos3': 'a lot of experience',
            'Somos4': ' when it comes to the ',
            'Somos5': 'design, development',
            'Somos6': ' and testing of technological solutions for small, medium and big companies on a wide range of market verticals. ',
            'Compromiso1': 'Commitment, Dedication, Talent and Expertise',
            'Compromiso2': ' are the main virtues of our team of professionals',
            'Valor': 'The added value that we provide on each one of the services that we offer comes from the commitment of our excellent team of professionals, who put their whole dedication, talent and expertise into the creation of solutions that have a great impact on our clients businesses.',
            'Carreras': 'Careers',
            'TextCarreras1': ' If you are passionate about building excellent web or mobile products, we would love to work\n with you.',
            'TextCarreras2': 'Here in 261ITSolutions we make ourselves proud to exceed our clients expectations through the design and development of customized products focused on the user.',
            'TextCarreras3': 'We are looking for developers who can showcase the following skillset:',
            'liCarreras1': 'Be able to implement efficient solutions.',
            'liCarreras2': 'Develop applications following the best coding practices.',
            'liCarreras3': 'Feel very comfortable working within a team.',
            'liCarreras4': 'Be willing to learn from other coworkers.',
            'liCarreras5': 'Have experience working with agile methodologies.',
            'Aplicar': 'Apply',
            'EnviaInfo': 'Send your information and CV to our email contact@261itsolutions.com',
            'Comencemos': 'Let’s start a project together',
            'Ingresa tu Email': 'Enter your email',
            'Enviar': 'Send',
            'Contacto': 'Contact Us',
            'contactEmail': 'contact@261itsolutions.com',
            'contactNumber': '+58 424 6711281',
            'contactDireccion': ' Av 5 de Julio con calle 72, Ediﬁcio Paraiso Oﬁcina 1b, Maracaibo, Edo Zulia.',
            'Venezuela': 'Venezuela',
            'derechosReservados': ' © 2020 261ITSolutions. All rights reserved.',
            'ConocerMas': 'Learn more',
            'Transformar0': 'To Transform digitally your',
            'Negocio': 'Business',
            'Producto': 'Product',
            'Emprendimiento': 'Startup',
        },
        'es': {
            'Servicio': 'Servicio',
            'Lenguaje': 'ES',
            'Herramientas': 'Herramientas',
            'Quienes-somos': 'Quiénes somos',
            'Carreras': 'Carreras',
            'Contactanos': 'Contáctanos',
            'Soluciones': 'Soluciones',
            'Tecnologicas': 'Tecnológicas',
            'Servicios': 'Servicios',
            'textServicios': '  Bien sea para un proyecto a corto o a largo plazo o una extensión de tu equipo interno de desarrollo, 261ITSoutions te ofrece una amplia gama de servicios para poder satisfacer tus necesidades de negocio en todo momento. Podemos construir tu producto desde cero o mejorar de manera significativa tus productos existentes.',
            'Outsourcing-de-Profesionales': 'Outsourcing de Profesionales',
            'textOutsourcing': ' Personal asignado a un cliente en particular en las áreas de Desarrollo de Software o Implementación de Infraestructura TI para la realización de las asignaciones requeridas, aumentando la productividad de los equipos internos de nuestros clientes.',
            'Desarrollo-Web': ' Desarrollo Web',
            'TextDesarrollo-Web': '   Desarrollo de aplicaciones web basadas en la usabilidad y el diseño responsive para proveer una experiencia enfocada en el usuario.',
            'Desarrollo-Movil': 'Desarrollo Móvil',
            'TextDesarrollo-Movil': 'Diseño y Construcción de aplicaciones móviles por parte de experimentados profesionales en el ámbito del desarrollo móvil tanto nativo como híbrido para iOS y Android.',
            'QA': 'QA (Pruebas de Software)',
            'TextQA': 'Automatización y ejecución de pruebas ágiles para garantizar la calidad y el correcto funcionamiento de las aplicaciones desarrolladas por nosotros y las de nuestros clientes.',
            'UXUI': 'UX/UI',
            'textUXUI': 'A traves de estrategia, diseño y tecnologia, creamos hermosas paginas web, apps y experiencias de marca que atraen a los consumidores de hoy en dia.',
            'BusinessIntelligence': 'Inteligencia de Negocio',
            'TextBusinessIntelligence': 'Ayudamos a nuestros clientes a construir ventajas competitivas mediante el fortalecimiento de sus estrategias de negocio y su toma de decisiones, todo sustentando en el correcto y apropiado entendimiento del verdadero valor de los datos.',
            'titleServicios2': 'Servicios Adicionales',
            'textServicios2': 'Texto de Servicios Adicionales 2',
            'MarketStudy': 'Estudio de Mercado',
            'textMarketStudy': 'Texto delservicio de Estudio de Mercado',
            'DigitalMarketing': 'Marketing Digital',
            'textDigitalMarketing': 'Texto de Servicio de Marketing Digital',
            'TextHerramientas': 'Nuestros Desarrolladores, Diseñadores, Analistas de QA y Especialistas en Infraestructura IT, son expertos en la utilización de las herramientas que les permiten construir soluciones tecnológicas que aceleran el crecimiento y la transformación digital de nuestros clientes.',
            'Amplia-experiencia': 'Amplia experiencia',
            'UX/UI': 'en Diseño UX/UI y Desarrollo orientado a',
            'MóvilYWeb': 'Móvil y Web',
            'Somos0': ' Somos una empresa de ',
            'Somos1': 'Soluciones IT',
            'Somos2': '. Contamos con ',
            'Somos3': 'amplia experiencia',
            'Somos4': ' en el ámbito del ',
            'Somos5': 'diseño, desarrollo',
            'Somos6': ' y pruebas de soluciones tecnológicas para pequeñas, medianas y grandes empresas en los diferentes sectores comerciales. ',
            'Compromiso1': 'Compromiso, Dedicación, Talento y Experticia',
            'Compromiso2': ' son las principales virtudes de nuestro equipo de profesionales',
            'Valor': 'El valor agregado que aportamos en cada uno de los servicios que proporcionamos proviene del compromiso de nuestro\n excelente equipo de profesionales, quienes siempre ponen a disposición su absoluta dedicación, talento y experticia para crear soluciones que impactan positivamente el negocio de nuestros clientes.',
            'Carrearas': 'Carreras',
            'TextCarreras1': ' Si te apasiona construir excelentes productos bien sea web o móviles, nos encantaría trabajar\n contigo.',
            'TextCarreras2': 'Aquí en 261ITSolutions nos orgullecemos en exceder las expectativas de nuestros clientes mediante el diseño y el desarrollo de productos enfocados en el usuario y hechos a la medida.',
            'TextCarreras3': 'El perfil que buscamos en un desarrollador es el siguiente: ',
            'liCarreras1': 'Ser capaz de implementar soluciones efectivas y eficientes.',
            'liCarreras2': 'Desarrollar aplicaciones siguiendo las mejores prácticas de codificación.',
            'liCarreras3': 'Sentirse cómodo trabajando en equipo.',
            'liCarreras4': 'Estar deseoso de obtener nuevos conocimientos de otros miembros del equipo.',
            'liCarreras5': 'Tener experiencia trabajando con metodologías ágiles.',
            'Aplicar': 'Aplicar',
            'EnviaInfo': 'Envia tu informacion y CV a nuestro correo',
            'Comencemos': 'Comencemos un nuevo proyecto juntos',
            'Ingresa tu Email': 'Ingresa tu Email',
            'Enviar': 'Enviar',
            'Contacto': 'Contacto',
            'contactEmail': 'contact@261itsolutions.com',
            'contactNumber': '+58 424 6711281',
            'contactDireccion': ' Av 5 de Julio con calle 72, Ediﬁcio Paraiso Oﬁcina 1b, Maracaibo, Edo Zulia.',
            'Venezuela': 'Venezuela',
            'derechosReservados': ' © 2020 261ITSolutions. Todos los derechos reservados.',
            'ConocerMas': 'Conocer más',
            'Transformar0': 'Para Transformar digitalmente tu',
            'Negocio': 'Negocio',
            'Producto': 'Producto',
            'Emprendimiento': 'Emprendimiento',
        },
        'ru': {
            'welcome': 'Добро пожаловать!',
            'message_from': '$1 {{gender:$3|отправил|отправила}} вам $2 {{plural:$2|сообщение|сообщений|сообщения}}. {{gender:$3|Он|Она}} ждёт ответа!'
        }
    });

    update_texts();
});